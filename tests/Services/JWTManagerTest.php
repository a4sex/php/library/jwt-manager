<?php

namespace A4Sex\Tests\Services;

use A4Sex\Services\JWKManager;
use A4Sex\Tests\Identity;
use A4Sex\Tests\User;
use A4Sex\Services\JWTManager;
use A4Sex\Tests\UserUid;
use A4Sex\Tests\UserUidIdentity;
use A4Sex\Tests\UserUidUuid;
use A4Sex\Tests\UserUuid;
use PHPUnit\Framework\TestCase;

class JWTManagerTest extends TestCase
{
    private $ttl = 600;
    private $user;
    private JWKManager $manager;

    public function setUp(): void
    {
        parent::setUp();
        $this->basePath = __DIR__;
        $this->manager = new JWKManager('../keys/', [["public.key"]]);
        $this->object = new JWTManager(
//            $this->manager,
            $this->basePath . '/../keys/private.key',
            $this->basePath . '/../keys/public.key',
            $this->ttl,
            'fs-issuer'
        );
        $this->user = new User();
    }

    public function testNoKeys()
    {
        $object = new JWTManager(
//            $this->manager,
            $this->basePath . '/../key5/private.key',
            $this->basePath . '/../keys/public.key',
            $this->ttl
        );
        self::expectException(\RuntimeException::class);
        $object->create();
    }

    public function testWrongKeysContent()
    {
        $object = new JWTManager(
//            $this->manager,
            $this->basePath . '/../keys/private5.key',
            $this->basePath . '/../keys/public.key',
            $this->ttl
        );
        self::expectException(\RuntimeException::class);
        $object->create();
    }

    public function testExpire()
    {
        self::assertEquals(time() + $this->ttl, $this->object->expire());
    }

    public function testCreate()
    {
        $token = $this->object->create($this->user);
        self::assertEquals(3, count(explode('.', $token)));
        $headers = new \stdClass();
        $payload = (array) $this->object->load($token, $headers);
        self::assertEquals(1, $payload['uid']);
        self::assertTrue(property_exists($headers, 'kid'));
    }

    public function testCreateWithoutUser()
    {
        $token = $this->object->create();
        $payload = (array) $this->object->load($token);
        self::assertNotNull($payload['exp']);
        self::assertNull(@$payload['uid']);
    }

    public function testLoad()
    {
        $token = $this->object->create($this->user);
        $payload = (array) $this->object->load($token);
        self::assertEquals('ROLE_USER', $payload['roles'][0]);
    }

    public function testCreateIdentity()
    {
        $token = $this->object->create(new Identity());
        $payload = (array) $this->object->load($token);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('uid', $payload);
        self::assertArrayNotHasKey('uuid', $payload);
    }

    public function testCreateUuid()
    {
        $token = $this->object->create(new UserUuid());
        $payload = (array) $this->object->load($token);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('uid', $payload);
        self::assertArrayNotHasKey('uuid', $payload);
    }

    public function testCreateUid()
    {
        $token = $this->object->create(new UserUid());
        $payload = (array) $this->object->load($token);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('uid', $payload);
        self::assertArrayNotHasKey('uuid', $payload);
        self::assertNotEquals($payload['id'], $payload['uid']);
    }

    public function testCreateUidUuid()
    {
        $token = $this->object->create(new UserUidUuid());
        $payload = (array) $this->object->load($token);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('uid', $payload);
        self::assertArrayHasKey('uuid', $payload);
        self::assertEquals($payload['id'], $payload['uid']);
        self::assertNotEquals($payload['uid'], $payload['uuid']);
    }

    public function testCreateUidIdentity()
    {
        $token = $this->object->create(new UserUidIdentity());
        $payload = (array) $this->object->load($token);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('uid', $payload);
        self::assertArrayHasKey('uuid', $payload);
        self::assertEquals($payload['id'], $payload['uid']);
        self::assertNotEquals($payload['uid'], $payload['uuid']);
    }

    public function testIssuer()
    {
        $token = $this->object->create($this->user);
        $payload = (array) $this->object->load($token);
        self::assertEquals('fs-issuer', $payload['iss']);
    }
}
