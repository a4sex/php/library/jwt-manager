<?php

namespace A4Sex\Services;

class JWKManager
{
    private array $keys = [];

    public function __construct(
        private string $keysDirectory,
        private ?array $keyFiles = null,
    ) {}

    public function getKeyFromAttributes(string $content, array $attributes): array
    {
        $key = [
          'kty' => $attributes['kty'] ?? 'RSA',
          'use' => $attributes['use'] ?? 'sig',
          'alg' => $attributes['alg'] ?? 'RS256',
          'n' => base64_encode($content),
          'e' => $attributes['e'] ?? 'AQAB',
        ];

        if (array_key_exists('kid', $attributes)) {
            $key['kid'] = $attributes['kid'];
        }

        return $key;
    }

    public function getKeyFromFile(
        string $filename,
        array $attributes
    ) {
        $filePath = $this->keysDirectory . $filename;
        if (!file_exists($filePath)) {
            throw new \RuntimeException('Key File not found: ' . $filePath);
        }

        $content = file_get_contents($filePath);

        return $this->getKeyFromAttributes($content, $attributes);
    }

    public function setKeysFromFiles(array $files): static
    {
        foreach ($files as $file) {
            if (!is_array($file)) {
                throw new \RuntimeException('Wrong file and attributes list format');
            }
            $attributes = [];
            $filename = $file[0];
            if (count($file) == 2) {
                $attributes = $file[1];
            }
            $key = $this->getKeyFromFile($filename, $attributes);
            $this->setKey($key);
        }

        return $this;
    }

    public function setKey(array $key): static
    {
        $this->keys[] = $key;

        return $this;
    }

    public function getKeys(): array
    {
        return [
            'keys' => $this->keys
        ];
    }

    public function keysFromFiles(?array $files = null): array
    {
        if (empty($this->keys)) {
            if (!$files and !$this->keyFiles) {
                throw new \RuntimeException('You need to specify `files` array');
            }
            $this->setKeysFromFiles($files ?? $this->keyFiles);
        }

        return $this->getKeys();
    }

}