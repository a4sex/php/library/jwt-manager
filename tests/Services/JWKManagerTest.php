<?php

namespace A4Sex\Tests\Services;

use A4Sex\Services\JWKManager;
use PHPUnit\Framework\TestCase;

class JWKManagerTest extends TestCase
{
    private JWKManager $object;

    protected function setUp(): void
    {
        parent::setUp();

        $this->object = new JWKManager(__DIR__ . '/../keys/', [["public.key"]]);
    }

    public function testKeysFromFilesDefault()
    {
        $keys = $this->object->keysFromFiles();
        self::assertArrayHasKey('keys', $keys);
        self::assertArrayHasKey('kty', $keys['keys'][0]);
        self::assertArrayHasKey('use', $keys['keys'][0]);
        self::assertArrayHasKey('n', $keys['keys'][0]);
        self::assertArrayNotHasKey('kid', $keys['keys'][0]);
    }

    public function testKeysFromFiles()
    {
        $keys = $this->object->keysFromFiles([['publicTest.key', ['kid' => '20230923']]]);
        self::assertArrayHasKey('keys', $keys);
        self::assertArrayHasKey('n', $keys['keys'][0]);
        self::assertArrayHasKey('kty', $keys['keys'][0]);
        self::assertEquals('20230923', $keys['keys'][0]['kid']);
    }

    public function testGetKeyFromFile()
    {
        self::markTestSkipped();
    }

    public function testSetKeysFromFiles()
    {
        self::markTestSkipped();
    }

    public function testGetKeys()
    {
        self::markTestSkipped();
    }

    public function testGetKeyFromAttributes()
    {
        self::markTestSkipped();
    }

    public function testSetKey()
    {
        self::markTestSkipped();
    }
}
